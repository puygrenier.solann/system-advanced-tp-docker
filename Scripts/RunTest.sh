#!/bin/bash
address="http://localhost"
portTomcat="8080"
portJenkins="8080/jenkins/"
LOCALHOST=$(curl -s $address:$portTomcat | grep "Apache Tomcat")
CURRENT_HOME=$(printenv HOME)

if [[ "$CURRENT_HOME" != "/root" ]];then 
	echo "(✕) : Not started with correct user root, current home directory is $CURRENT_HOME"
	exit 1
fi

echo "(✓) Correct home directory"

if [[ -z "$LOCALHOST" ]];then
	echo "(✕) : Not any Tomcat server at $address:$portTomcat"	
	exit 1
fi


echo "(✓) Tomcat server exist"
LOCALHOST=$(curl -s $address:$portJenkins | grep "Jenkins")

if [[ -z "$LOCALHOST" ]]; then
	echo "(✕): Not any Jenkins server at $LOCALHOST"
	exit 1
fi


echo "(✓) Jenkins server exist"

exit 0

