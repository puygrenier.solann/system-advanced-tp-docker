FROM tomcat:9.0-slim
USER root
#RUN apt-get update && apt-get install -y lsb-release
RUN mkdir /docker-data
COPY ./jenkins.war /docker-data
VOLUME /docker-data
EXPOSE 8080
EXPOSE 8080
RUN sed -i 's/SSLEnabled="false"/SSLEnabled="true"/' conf/server.xml 
RUN mv /docker-data/jenkins.war ./webapps/ 



RUN ./bin/catalina.sh start


